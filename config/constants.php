<?php

return [
    'default'     => [
        'DefaultId' => 0,
        'AdminId'   => [1],
        'Limit'     => 20,
    ],
    'Device'      => [
        'ios'     => 'ios',
        'android' => 'android',
    ],
    'Errors'      => [
        'database'       => 'Error While Inserting',
        'unauthorized'   => 'Unauthorized',
        'loginError'     => 'There was an error with your E-Mail/Password combination. Please try again.',
        'GetAccessToken' => 'Given Refresh Token is Invalid',
    ],
    'Success'     => [
        'Login' => 'LoginSuccess',
    ],
    'ServiceName' => [
        'Login'      => 'Login-Service',
        'Physicians' => 'Physicians-Service',
    ],
];
