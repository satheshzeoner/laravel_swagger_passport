<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*
|----------------------------------------------------------------------
|    LOGIN AND REGISTRATION
|----------------------------------------------------------------------
|
*/

/* User Credentials Checking */
Route::post('login', 'API\AuthController@login')
    ->name('login');

/* RefreshToken Based Access Token */
Route::get('access-token', 'API\AuthController@getAccessToken');


Route::group(['middleware' => ['auth:api']], function () {

    /* Get user Details */
    Route::get('user-details', 'API\User\UserController@getUserList');

    /* Get physicians */
    Route::get('get-physicians', 'API\Physicians\PhysiciansController@getPhysiciansList');
});
