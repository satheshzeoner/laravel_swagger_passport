<?php

namespace App\Http\Controllers\API;

use Exception;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client as OClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->user_model = new User();
    }

    /**
     * @OA\Post(
     * path="/login",
     * summary="Sign in",
     * description="Login by email, password",
     * operationId="login",
     * tags={"Users"},
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\Response(
     *    response=422,description="Wrong credentials response",
     *     )
     * )
     */


    # =============================================
    # =           Get User Login                  =
    # =============================================

    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            return Helper::getTokenAndRefreshToken(request('email'), request('password'));
        } else {
            $result = Helper::customError(401, config('constants.Errors.loginError'), $user = ['email' => request('email'), 'password' => request('password')], config('constants.ServiceName.Login'));
            return response()->json($result, 200);
        }
    }

    /**
     * @OA\Get(
     * path="/access-token",
     * summary="Get Access Token By Passing Refresh Token",
     * description="Login by email, password",
     * operationId="access-token",
     * tags={"Users"},
     *     @OA\Parameter(
     *         name="refresh_token",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\Response(
     *    response=404,description="invalid url",
     *     )
     * )
     */

    # =============================================
    # =           Get AccessToken                 =
    # =============================================

    public function getAccessToken(Request $request)
    {
        $paramDetails = $request->all();

        $validator = Validator::make($request->all(), [
            'refresh_token' => 'required'
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Login'));
            return response()->json($result, 200);
        }

        $oClient = OClient::where('password_client', 1)->first();
        try {
            return Helper::getRefreshTokenbasedAccess($oClient, $paramDetails['refresh_token']);
        } catch (Exception $e) {
            $result = Helper::customError(401, config('constants.Errors.GetAccessToken'), config('constants.ServiceName.Login'));
            return response()->json($result, 200);
        }
    }
}
