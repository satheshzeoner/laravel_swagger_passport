<?php

namespace App\Http\Controllers\API\Physicians;

use Exception;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/* Service */

use App\Services\Physicians\PhysiciansService;

class PhysiciansController extends Controller
{

    /**
     * @var PhysiciansService
     */
    private $PhysiciansService;

    public function __construct()
    {
        $this->PhysiciansService = new PhysiciansService();
    }

    /**
     * @OA\Get(
     * path="/get-physicians",
     * security = { { "Bearer": {} } },
     * summary="get-physicians",
     * description="get-physicians",
     * operationId="get-physicians",
     * tags={"physicians"},
     * @OA\Response(
     *    response=422,description="Wrong credentials response",
     *     )
     * )
     */

    # =============================================
    # =           Get physicians List             =
    # =============================================

    public function getPhysiciansList(request $request)
    {
        $userId   = Auth::user()->id;
        $paramDetails   = $request->all();
        $Response = $this->PhysiciansService->getPhysiciansList($userId,$paramDetails);
        return Helper::customResponse(200, $Response, '', config('constants.ServiceName.Physicians'));
    }

}
