<?php

namespace App\Http\Controllers\API\User;

use Exception;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/* Service */

use App\Services\Users\UsersService;

class UserController extends Controller
{

    /**
     * @var UsersService
     */
    private $UsersService;

    public function __construct()
    {
        $this->UsersService = new UsersService();
    }

    /**
     * @OA\Get(
     * path="/user-details",
     * security = { { "Bearer": {} } },
     * summary="user-list",
     * description="user-list",
     * operationId="user-list",
     * tags={"Users"},
     * @OA\Response(
     *    response=422,description="Wrong credentials response",
     *     )
     * )
     */

    # =============================================
    # =           Get User List                  =
    # =============================================

    public function getUserList()
    {
        $userId = Auth::user()->id;
        $Response                = $this->UsersService->getUserList($userId);
        return Helper::customResponse(200, $Response, '', config('constants.ServiceName.Login'));
    }

}
