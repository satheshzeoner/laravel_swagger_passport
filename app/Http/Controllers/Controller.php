<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations\OpenApi;


/**
 *
 * @OA\SecurityScheme(
 *      securityScheme="Bearer",
 *      in="header",
 *      name="Authorization",
 *      type="http",
 *      scheme="Bearer",
 * ),
 *
 * @OA\OpenApi(
 *     @OA\Server(
 *         url="http://localhost/projects/firstcovid_api/api/",
 *         description="API server"
 *     ),

 *     @OA\Info(
 *         version="1.0.0",
 *         title="DDL Schedule",
 *         description="DDL Schedule",
 *         termsOfService="http://swagger.io/terms/",
 *         @OA\Contact(name="Swagger API Team"),
 *         @OA\License(name="MIT")
 *     ),
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
