<?php

namespace App\Services\Users;

use App\Helpers\Helper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Exception;


/* Repository */

use App\Repositories\Users\UsersRepositories;

class UsersService
{
    public function __construct()
    {
        $this->UsersRepositories = new UsersRepositories();
    }

    # =============================================
    # =              GET Details                  =
    # =============================================

    public function getUserList($userId)
    {
        try {

            $userDetails       = $this->UsersRepositories->getUserList($userId);
            $clientDetails     = ($userDetails != '') ? $this->UsersRepositories->getUserClients($userDetails, $userId) : '';
            $faclityDetails    = ($clientDetails != '') ? $this->UsersRepositories->getFacility($clientDetails, $userId) : '';
            $physiciansDetails = ($clientDetails != '') ? $this->UsersRepositories->getPhysicians($clientDetails, $userId) : '';

            $user['user_details']       = Helper::setData($userDetails);
            $user['client_details']     = Helper::setData($clientDetails);
            $user['facility_details']   = Helper::setData($faclityDetails);
            $user['physicians_details'] = Helper::setData($physiciansDetails);

            return $user;
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =        DO ERROR FORMAT Details            =
    # =============================================

    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
