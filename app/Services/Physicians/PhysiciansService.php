<?php

namespace App\Services\Physicians;

use App\Helpers\Helper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Exception;


/* Repository */

use App\Repositories\Physicians\PhysiciansRepositories;

class PhysiciansService
{
    public function __construct()
    {
        $this->PhysiciansRepositories = new PhysiciansRepositories();
    }

    # =============================================
    # =              GET Details                  =
    # =============================================

    public function getPhysiciansList($userId,$paramDetails)
    {
        try {

            $physiciansDetails              = $this->PhysiciansRepositories->getPhysiciansList($userId,$paramDetails);

//            $user['physicians_details']     = Helper::setData($physiciansDetails);
            $user['physicians_details']     = $physiciansDetails;

            return $user;
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =       Users Add Details                   =
    # =============================================

    public function storeUserDetails(Request $request)
    {
        try {
            $userDetails                     = $request->all();
            $userDetails['user']['password'] = Hash::make($userDetails['user']['password']);
            return $this->UsersRepositories->storeUser($userDetails['user']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =           Edit Details                    =
    # =============================================

    public function getUserSpecificDetails($masterId)
    {
        try {
            return $this->UsersRepositories->getSpecificUserDetails($masterId);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                  Update                  =
    # =============================================

    public function UpdateDetails(request $request)
    {
        try {
            $users    = $request->all();
            $masterId = $users['master_id'];
            return $this->UsersRepositories->updateUsers($masterId, $users['user']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =             Destroy                       =
    # =============================================

    public function destroyUserDetails(request $request)
    {
        $postDetails = $request->all();
        try {
            return $this->UsersRepositories->destroyUser($postDetails['masterid']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
