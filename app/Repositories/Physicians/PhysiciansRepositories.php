<?php

namespace App\Repositories\Physicians;


/* Models */

use App\Models\Physicians\Physicians;
use Illuminate\Support\Facades\Log;

class PhysiciansRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get User Details */
    public function getPhysiciansList($userId, $paramDetails)
    {

        $physicians = Physicians::orderby('name', 'asc');
        $physicians = $physicians->limit(15);
        $physicians = $physicians->select('id', 'name', 'client_bill_to');
        $physicians = $physicians->where('name', 'like', '%' . $paramDetails['search'] . '%');
        $physicians = $physicians->where('active', '=', 1);
        $physicians = $physicians->get();

        return $physicians;
    }


    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
