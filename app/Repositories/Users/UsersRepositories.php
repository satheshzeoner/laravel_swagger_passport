<?php

namespace App\Repositories\Users;


/* Models */

use App\Models\Users\UserMember;
use App\Models\Client\Clients;
use App\Models\Facility\Facility;
use App\Models\Physicians\Physicians;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UsersRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get User Details */
    public function getUserList($userId)
    {
        return UserMember::select('id','name','email','created_at','updated_at','client')
            ->whereId($userId)->first();
    }

    /* Get Client Details */
    public function getUserClients($userDetails,$userId)
    {
        if(in_array($userId, config('constants.default.AdminId'))){
            return Clients::all();
        }
        return ($userDetails->client != '') ? Clients::whereId($userDetails->client)->first() : '';
    }

    /* Get Facility Details */
    public function getFacility($clientDetails,$userId)
    {
        if(in_array($userId, config('constants.default.AdminId'))){
            return Facility::all();
        }
        $facility = Facility::whereId($clientDetails->facility)->first();
        return ($clientDetails->facility != '') ? Facility::whereId($clientDetails->facility)->first() : '';
    }

    /* Get Physicians Details */
    public function getPhysicians($clientDetails,$userId)
    {

        if(in_array($userId, config('constants.default.AdminId'))){
            return Physicians::limit(config('constants.default.Limit'))->get();
        }
        $physicians = Physicians::orderby('name', 'asc');
        $physicians = $physicians->select('id', 'name', 'client_bill_to');
        $physicians = $physicians->where('active', '=', 1);
        $physicians = $physicians->where('client_bill_to', $clientDetails->bill_to_code);
        $physicians = $physicians->first();

        return ($physicians != '') ? $physicians : '';
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
