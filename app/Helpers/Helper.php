<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Client as OClient;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
//use App\Models\Auth\AuthUser;

class Helper
{

    public static function unauthenticatedAction($request = null)
    {
        $uri = url()->current();
        $message['clientStatusCode']          = 401;
        $message['error']['message']          = 'unauthorized';
        $message['uri']                       = $uri;

        Helper::logError('unauthorized', config('constants.Code.Unauthorized'), $request, $_SERVER['REQUEST_URI']);

        return response()->json($message, 401);
    }

    public static function customError($status_code, $data, $request = null, $service = null, $code = null)
    {
        $message['clientStatusCode'] = $status_code;
        $message['error']['message'] = $data;
        $message['uri']              = url()->current();

        Helper::logError($data, $status_code, $request, $service);

        return $message;
    }

    public static function customResponse($status_code, $data, $request = null, $service = null, $code = null)
    {
        $userId                      = Auth::user()->id;
        $message['clientStatusCode'] = $status_code;
        $message['user_id']          = $userId;
        $message['data']             = $data;
        $message['uri']              = url()->current();

        Helper::logSuccess($data, $status_code, $request, $service, $userId);

        return $message;
    }
    public static function customUpdatedResponse($status_code, $message, $request = null, $service = null, $code = null)
    {
        $userId                       = Auth::user()->id;
        $companyId                    = AuthUser::find($userId)->userProfile->company_id;
        $response['clientStatusCode'] = $status_code;
        $response['user_id']          = $userId;
        $response['company_id']       = $companyId;

        $response['message']          = $message;
        $response['uri']              = url()->current();

        Helper::logSuccess($response, $status_code, $request, $service, $userId);

        return $response;
    }

    public static function customResponsePagination($status_code, $data, $paginationData, $request = null, $service = null, $code = null)
    {
        $userId                      = Auth::user()->id;
        $companyId                   = AuthUser::find($userId)->userProfile->company_id;
        $message['clientStatusCode'] = $status_code;
        $message['user_id']          = $userId;
        $message['company_id']       = $companyId;

        $message['data']            = $data;
        $message['firstItemOnPage'] = $paginationData['firstItemOnPage'];
        $message['hasNextPage']     = $paginationData['hasNextPage'];
        $message['next_page_url']   = $paginationData['next_page_url'];
        $message['prev_page_url']   = $paginationData['prev_page_url'];
        $message['first_page_url']  = $paginationData['first_page_url'];
        $message['last_page_url']   = $paginationData['last_page_url'];
        $message['hasPreviousPage'] = $paginationData['hasPreviousPage'];
        $message['isFirstPage']     = $paginationData['isFirstPage'];
        $message['isLastPage']      = $paginationData['isLastPage'];
        $message['lastItemOnPage']  = $paginationData['lastItemOnPage'];
        $message['pageCount']       = $paginationData['pageCount'];
        $message['pageNumber']      = $paginationData['pageNumber'];
        $message['pageSize']        = $paginationData['pageSize'];
        $message['totalItemCount']  = $paginationData['totalItemCount'];
        $message['uri']             = url()->current();

        Helper::logSuccess($data, $status_code, $request, $service, $userId);

        return $message;
    }

    /* Success Log Notification */
    public static function logSuccess($message, $statuscode, $requestdetails = null, $service = null, $userId = null)
    {
        $logInfo['statusCode'] = $statuscode;
        $logInfo['userId']     = $userId;
        $logInfo['message']    = $message;
        $logInfo['service']    = $service;

        if (!empty($requestdetails)) {
            $logInfo['params']    = $requestdetails;
        } else {
            $logInfo['params']    = 'No Params';
        }

        $logInfo['uri']    = url()->current();

        Log::notice($logInfo);
    }

    /* Error Log Notification */
    public static function logError($message, $statuscode, $requestdetails = null, $service = null)
    {
        $logInfo['statusCode'] = $statuscode;
        $logInfo['service']    = $service;
        $logInfo['message']    = $message;

        if (!empty($requestdetails)) {
            $logInfo['params'] = $requestdetails;
        } else {
            $logInfo['params'] = 'No Params';
        }

        $logInfo['uri']        = url()->current();

        Log::error($logInfo);
    }

    public static function getIpRelatedDetails()
    {
        $ip                    = file_get_contents("http://ipecho.net/plain");
        $url                   = 'http://ip-api.com/json/' . $ip;
        $GettingIpBasedDetails = file_get_contents($url);
        return json_decode($GettingIpBasedDetails);
    }

    public static function make_array($value)
    {
        return $value = collect($value)->map(function ($x) {
            return (array) $x;
        })->toArray();
    }

    public static function getTokenAndRefreshToken($email, $password)
    {
        $oClient = OClient::where('password_client', 1)->first();
        $http = new Client;
        $response = $http->request('POST', env('APP_URL') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $email,
                'password' => $password,
                'scope' => '*',
            ],
        ]);

        $result = json_decode((string) $response->getBody(), true);

        $message['clientStatusCode'] = 200;
        $message['data']['token_type']       = $result['token_type'];
        $message['data']['expires_in']       = $result['expires_in'];
        $message['data']['access_token']     = $result['access_token'];
        $message['data']['refresh_token']    = $result['refresh_token'];
        $message['uri']                      = url()->current();

        Log::notice($message);

        /* Log */
        Helper::logSuccess('Login Success', 200, $request = ['Email' => $email, 'Password' => $password], config('constants.ServiceName.Login'));

        return response()->json($message, 200);
    }

    public static function getRefreshTokenbasedAccess(OClient $oClient, $refresh_token)
    {
        $oClient = OClient::where('password_client', 1)->first();
        $http = new Client;
        $response = $http->request('POST', env('APP_URL') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'refresh_token' => $refresh_token,
                'scope' => '*',
            ],
        ]);

        $result = json_decode((string) $response->getBody(), true);
        return response()->json($result, 200);
    }

    public static function Current_url()
    {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
            $link = "https";
        else
            $link = "http";

        // Here append the common URL characters.
        $link .= "://";

        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];

        // Append the requested resource location to the URL
        $link .= $_SERVER['REQUEST_URI'];

        // Print the link
        return $link;
    }
    public static function customPagination($result)
    {
        return [
            'firstItemOnPage' => $result['from'],
            'next_page_url'   => $result['next_page_url'],
            'prev_page_url'   => $result['prev_page_url'],
            'first_page_url'  => $result['first_page_url'],
            'last_page_url'   => $result['last_page_url'],
            'hasNextPage'     => ($result['next_page_url'])  ? true : false,
            'hasPreviousPage' => ($result['prev_page_url'])  ? true : false,
            'isFirstPage'     => ($result['first_page_url']) ? true : false,
            'isLastPage'      => ($result['last_page_url'])  ? true : false,
            'lastItemOnPage'  => $result['to'],
            'pageCount'       => $result['last_page'],
            'pageNumber'      => $result['current_page'],
            'pageSize'        => $result['per_page'],
            'totalItemCount'  => $result['total'],
        ];
    }

    /* Null to String Convert */
    public static function setData($value)
    {
        $details = collect($value)->toArray();

        foreach ($details as $key => $value){
            $output[$key] = is_null($value)? '': $value;
        }

        return($output);
    }

    public static function setInnerData($value)
    {
        $details = collect($value)->toArray();

        foreach ($details as $mainKey => $value){
            foreach ($value as $innerKey => $innerValue){
                $output[$mainKey][$innerKey] = is_null($innerValue)? '': $innerValue;
            }
        }

        return($output);
    }



}
