<?php

namespace App\Exceptions;

use App\Helpers\Helper;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        /* Un-Auth Token Validation */
        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            $serverTokenDetail      = collect($request->server);
            $BearerToken            = (isset($serverTokenDetail['REDIRECT_HTTP_AUTHORIZATION'])) ? $serverTokenDetail['REDIRECT_HTTP_AUTHORIZATION'] : '';
            $resultDetails['Token'] = $BearerToken;
            $resultDetails['uri']   = url()->current();
            Log::info($resultDetails);
            Log::info($exception);
            return Helper::unauthenticatedAction($request->all());
        }

        /* Given Method {GET,POST..} Validation */
        if ($exception instanceof MethodNotAllowedHttpException) {

            $message['clientStatusCode']          = 401;
            $message['error']['code']             = 1600;
            $message['error']['message']          = 'Given ' . $request->method() . ' Method is Invalid for Access Current Api';
            $message['uri']                       = url()->current();
            Log::error($message);
            return response()->json($message, 401);
        }

        return parent::render($request, $exception);
    }
}
