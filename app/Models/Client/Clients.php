<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;


class Clients extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'client_number',
            'name',
            'alpha_lookup',
            'address1',
            'address2',
            'city',
            'state',
            'zip',
            'provider',
            'salesman',
            'route',
            'client_id1',
            'client_id2',
            'clientid3',
            'state_license',
            'bill_to_code',
            'created_at',
            'updated_at',
            'deleted_at',
            'additional',
            'custom_order',
            'facility',
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

}
